<?php
require_once "admin_page.inc" ;
require_once "dba.php" ;
require_once "validate.inc" ;
require_once "mkpage.inc" ;

class s_edit_page extends admin_page {
	
	//リストページ表示
	function On_default($p) {
		$a = new api()  ;
//		$p{"s"} = "o" ;
		$cat = $p['get']['cat'] ;
		$d = array('l'=>$a->chron_list($p['get'])) ;
		
		for($i=0;$i<count($d['l']);$i++) {

		}
//		print_r($d) ;
		return array('chron_l'.$cat.'_t.html',array_merge($d,$this->admin)) ;
	}
	
	//編集ページ表示
	function On_edit($p) {

		$id = $p['get']['id'] ;
		$a = new api() ;
		$db = new Table_cms_chron ;
		$d = $a->get_chron($id) ;
		$d['type'] = array($d['type']=>1) ;
//print_r($d) ;
		return $this->editpage($d,array('edit'=>1)) ;
	}
	
	//新規ページ表示
	function On_new($p) {
		$d = array('cat'=>$p['get']['cat'],'content'=>array('stat'=>1)) ;
		return $this->editpage($d,array('new'=>1)) ;
	}		
	
	function editpage($d,$o) {

		$db = new Table_cms_chron ;
		$a = new api() ;
		$f = $this->mkform($d) ;
		$p = array('f'=>$f,'d'=>$d) ;
//print_r($p) ;
		return array('chron_e'.$d['cat'].'_t.html',array_merge($p,$o,$this->admin));	
	}
	
	//更新フォームデータ生成
	function mkform($d) {
		$tb = new Table_cms_chron ;
		$a = new api() ;
		$f = array() ;
		$c = $d['content'] ;

		$f['year'] = $this->mkftag('text','d[year]',$d['year'], array('size'=>5)) ;
		$f['month'] = $this->mkftag('text','d[month]',$d['month'], array('size'=>5)) ;
		$f['day'] = $this->mkftag('text','d[day]',$d['day'], array('size'=>5)) ;		
		$f['dekigoto'] = $this->mkftag('textarea','c[dekigoto]',$c['dekigoto'],array('cols'=>40,'row'=>4)) ;
		$f['jinji'] = $this->mkftag('textarea','c[jinji]',$c['jinji'],array('cols'=>40,'row'=>4)) ;
		$f['shohin'] = $this->mkftag('textarea','c[shohin]',$c['shohin'],array('cols'=>40,'row'=>4)) ;
		$f['sonota'] = $this->mkftag('textarea','c[sonota]',$c['sonota'],array('cols'=>40,'row'=>4)) ;
		$f['jiken'] = $this->mkftag('textarea','c[jiken]',$c['jiken'],array('cols'=>40,'row'=>4)) ;
		$f['gyoukai'] = $this->mkftag('textarea','c[gyoukai]',$c['gyoukai'],array('cols'=>40,'row'=>4)) ;
		$f['hit'] = $this->mkftag('textarea','c[hit]',$c['hit'],array('cols'=>40,'row'=>4)) ;		 
		return $f ;
	}	

	//update/insert処理

	function On_doupdate($p) {
//		echo "update" ;
		$p = $p['post'] ;
//		print_r($p);
		$this->update($p) ;
		header("Location:e_chron.php?cat=".$p['d']['cat']) ;
	}

	function validate($p)  {
		$valid  = new validate(0) ;
		$cont = $p['c'] ;
		$vm = array() ;
		$wm = array() ;
		$c = array() ;
		foreach($cont as $k=>$v) {
			$v = $valid->conv_fld($k,$v) ;
			$ck = $valid->chk_fld($m,$k,$v,$cont) ;
			if($ck==1) {
				$vm[$k] = $m ;
			} else if($ck==2) {
				$wm[$k] = $m ;
			}
			$c[$k] = $v ;
		}
//		print_r($c); 
		if(count($vm)>0) {	
			$p['content'] = $cont ;
			$r = array('msg'=>join("<br/>",$vm)) ;
			if($p['id']=="") $r['new'] = 1 ;
			else $r['edit'] = 1 ;
			return $this->editpage($p,$r) ;
		}
		return null ;
	}
	function update($p) {
//echo "<pre>";print_r($p) ;echo "</pre>" ;
		$db = new Table_cms_chron ;
		$a = new api ;
		$id = $p['id'] ;
		if($p['c']['stat']==4) { //delete
			$db->delete_record($id) ;
			return ;
		}

		if($id==0) {
			$id = $db->insert(array('stat'=>1,'content'=>array())) ;
		} else {
			$r = $db->query_record("*",$id,"id") ;
		}
		$q = $p['d'] ;
		$q['uid'] = $_SESSION['cred']['auth']['serial'] ;
		$q['content']=$p['c'] ;
//		print_r($q) ;
		$db->update($q,$id) ;	

		return ;
	}

	function On_delete($p) {
		$id = intval($p['get']['id']) ;
		if($id>0) {
			$db = new Table_cms_news() ;
			$r = $db->query_record("*",$id) ;
			$db->delete_record($id) ;
			$pa = new mkpage ;
			$pa->page_del($r['pageid']) ;
//			$pa->top() ;
		}
		header("Location:e_chron.php") ;
		return ;		
	}
	function On_cancel($p) {
		header("Location:e_chron.php?cat=".$p['get']['cat']) ;
		return ;		
	}
}
$o = new s_edit_page ;
$o->dispatch();