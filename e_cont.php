<?php
require_once "admin_page.inc" ;
require_once "dba.php" ;
require_once "validate.inc" ;
require_once "mkpage.inc" ;

class s_edit_page extends admin_page {
	
	//リストページ表示
	function On_default($p) {
		$a = new api()  ;
//		$p{"s"} = "o" ;
		$cat = $p['get']['cat'] ;
		$l = $a->cont_list($p['get']) ;

		
		for($i=0;$i<count($l);$i++) {
			$c = $l[$i]['content'] ;
			if($c['p1']) {
				$l[$i]['photos'] = '<a href="'.$GLOBALS['IMAGE_PATH'].$this->fname($l[$i]['id'],"p1","jpg").'" target="_blank">'.$c['p1'].'</a>' ;
			}
			$l[$i]['text'] = mb_substr($c['text'],0,20)."…" ;
		}
		$d = array('l'=>$l);
//		print_r($d) ;
		return array('cont_l'.$cat.'_t.html',$d) ;
	}
	
	//編集ページ表示
	function On_edit($p) {
		$id = $p['get']['id'] ;
		$a = new api() ;
		$db = new Table_cms_cont ;
		$d = $a->get_cont($id) ;
		$d['type'] = array($d['type']=>1) ;
//print_r($d) ;
		return $this->editpage($d,array('edit'=>1)) ;
	}
	
	//新規ページ表示
	function On_new($p) {
		$d = array('cat'=>$p['get']['cat'],'content'=>array('stat'=>1)) ;
		return $this->editpage($d,array('new'=>1)) ;
	}		
	
	function editpage($d,$o) {
		$db = new Table_cms_cont ;
		$a = new api() ;
		$f = $this->mkform($d) ;
//print_r($d) ;
		if($d['content']['p1']) {
			$f['photos'] = '<a href="'.$GLOBALS['IMAGE_PATH'].$this->fname($d['id'],"p1","jpg").'" target="_blank">'.$d['content']['p1'].'</a>'.$f['photos'] ;
		}
		if($d['content']['f1']) {
			$pi = pathinfo($d['content']['f1']) ;
			$f['files'] = '<a href="'.$GLOBALS['FILE_PATH'].$this->fname($d['id'],"f1",$pi['extension']).'" target="_blank">'.$d['content']['f1'].'</a>'.$f['files'] ;
		}
		$p = array('f'=>$f,'d'=>$d) ;
//print_r($p) ;
		return array('cont_e'.$d['cat'].'_t.html',array_merge($p,$o,$this->admin));	
	}
	
	//更新フォームデータ生成
	function mkform($d) {
		$tb = new Table_cms_cont ;
		$a = new api() ;
		$f = array() ;
		$c = $d['content'] ;
		
		$f['date'] = $this->mkftag('text','c[date]',$c['date'],array('class'=>"datep")) ;
		$f['target'] = $this->mkftag('text','c[target]',$c['target'],array('size'=>20)) ;
		$f['tanto'] = $this->mkftag('text','c[tanto]',$c['tanto'],array('size'=>20)) ;
		$f['text'] = $this->mkftag('textarea','c[text]',$c['text'],array('cols'=>40,'rows'=>10)) ;
		$f['photos'] = $this->mkftag("file",'p1');
		$f['files'] = $this->mkftag("file",'f1');

		$f['comment'] = $this->mkftag('textarea','c[comment]',$c['comment'],array('cols'=>40,'rows'=>2)) ;
		return $f ;
	}	

	//update/insert処理

	function On_doupdate($p) {
//		echo "update" ;
		$p = $p['post'] ;
//		print_r($p);
		$this->update($p) ;
		header("Location:e_cont.php?cat=".$p['d']['cat']) ;
	}

	function validate($p)  {
		$valid  = new validate(0) ;
		$cont = $p['c'] ;
		$vm = array() ;
		$wm = array() ;
		$c = array() ;
		foreach($cont as $k=>$v) {
			$v = $valid->conv_fld($k,$v) ;
			$ck = $valid->chk_fld($m,$k,$v,$cont) ;
			if($ck==1) {
				$vm[$k] = $m ;
			} else if($ck==2) {
				$wm[$k] = $m ;
			}
			$c[$k] = $v ;
		}
//		print_r($c); 
		if(count($vm)>0) {	
			$p['content'] = $cont ;
			$r = array('msg'=>join("<br/>",$vm)) ;
			if($p['id']=="") $r['new'] = 1 ;
			else $r['edit'] = 1 ;
			return $this->editpage($p,$r) ;
		}
		return null ;
	}
	function update($p) {
//echo "<pre>";print_r($p) ;echo "</pre>" ;
		$db = new Table_cms_cont ;
		$a = new api ;
		$id = $p['id'] ;
		if($p['c']['stat']==4) { //delete
			$db->delete_record($id) ;
			return ;
		}
		$c = $p['c'] ;

		if($id==0) {
			$id = $db->insert(array('stat'=>1,'content'=>array())) ;
		} else {
			$r = $db->query_record("*",$id,"id") ;
		}
		$c['p1'] = $r['content']['p1'] ;
		$c['f1'] = $r['content']['f1'] ;
		
		$p1 = $this->upfile("p1",$id); if($p1) $c['p1'] = $p1 ;
		$f1 = $this->upfile("f1",$id) ; if($f1) $c['f1'] = $f1 ;
		
		$q = $p['d'] ;
		$q['content']=$c ;
		$q['uid'] = $_SESSION['cred']['auth']['serial'] ;
//		print_r($q) ;
		$db->update($q,$id) ;	

		return ;
	}
	function fname($id,$num,$ext) {
		return $id."_".$num.".".$ext ;
	}
	function upfile($name,$id) {
		$ret = null  ;
//print_r($_FILES) ;
		if($_FILES[$name] && $_FILES[$name]['error']==0) {
			$pi = pathinfo($_FILES[$name]['name']) ;
			$ext = strtolower($pi['extension']) ;
			if($ext=="jpeg") $ext = "jpg" ;
			if($ext=="jpg") {
				$fn = $this->fname($id,$name,$ext) ;
				$td = $GLOBALS['UPLOAD_IMAGE_PATH'].$fn ;
				copy($_FILES[$name]['tmp_name'],$td) ;	
//				echo "upload $fn" ;				
			} else {
				$fn = $this->fname($id,$name,$ext) ;
				$td = $GLOBALS['UPLOAD_FILE_PATH'].$fn ;
				copy($_FILES[$name]['tmp_name'],$td) ;	
//				echo "upload $fn" ;	
			}
			$ret = $_FILES[$name]['name'] ;	
		} 
		return $ret ;
	}
	function On_delete($p) {
		$id = intval($p['get']['id']) ;
		if($id>0) {
			$db = new Table_cms_news() ;
			$r = $db->query_record("*",$id) ;
			$db->delete_record($id) ;
			$pa = new mkpage ;
			$pa->page_del($r['pageid']) ;
//			$pa->top() ;
		}
		header("Location:e_cont.php") ;
		return ;		
	}
	function On_cancel($p) {
		header("Location:e_cont.php?cat=".$p['get']['cat']) ;
		return ;		
	}
}
$o = new s_edit_page ;
$o->dispatch();