<?php
$pathinfo = pathinfo($_SERVER['PHP_SELF']);
$self_name=$pathinfo['basename'] ;
$dir_name = $pathinfo['dirname'] ;
if($dir_name!="/") $dir_name = $dir_name."/" ;
$location_pc = 'Location: '.$dir_name.$self_name;
$location_mobile = 'Location: /mobile'.$dir_name.$self_name;
$ua = $_SERVER['HTTP_USER_AGENT'];
if (((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) && strpos($dir_name,'mobile')===false ) {
  header($location_mobile);
  exit();
}
?>