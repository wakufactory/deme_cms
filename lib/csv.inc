<?php
class csv {
	var $data = array() ;
	var $head ;
	var $_put ;
	var $param = array('delimiter'=>",",'charset'=>"",'BOM'=>false, 'header'=>true,'newline'=>"\n",'quote'=>"\"",'lastnl'=>false) ;
	function setHead($h) {
		if($this->is_hash($h)) {
			$this->head = $h ;
		} else {
			$this->head = array() ;
			for($i=0;$i<count($h);$i++) {
				$this->head[$h[$i]] = $h[$i] ;
			}
		}
	}
	
	function setLine($d) {
		if($this->is_hash($d)) {
			$this->data[] = $d ;
		} else {
			$data = array() ;
			foreach($this->head as $k=>$v) {
				$data[$v] = array_shift($d) ;
			}
			$this->data[] =$data ;
		}
	}
	
	function _putCSV($p=null ) {
		if($p) $this->param = array_merge($this->param,$p) ;
		$l = array() ;
			$fn = $this->_put ;
		if($this->param['header']) {
			foreach($this->head as $k=>$v) {
				$l[] = $this->_esc($k) ;
			}
			$fn(join($this->param['delimiter'],$l).$this->param['newline'] );
		}
		$len = count($this->data) ;
		for($i=0;$i<$len;$i++) {
			$data = $this->data[$i] ;
			$l = array() ;
			foreach($this->head as $k=>$v) {
				$l[] = $this->_esc($data[$v]) ;
			}
			$fn(join($this->param['delimiter'],$l).
				(($this->param['lastnl']||$i<$len-1)?$this->param['newline']:"") );
		}
	}
	function _esc($data) {
		if($this->param['charset']!="") $data = mb_convert_encoding($data,$this->param['charset'],"UTF-8") ;
		if($this->param['quote']!="") return $this->param['quote'].str_replace('"','""',$data).$this->param['quote'] ;
		else return $data ;
	}
	function showCSV($p=null) {
		$this->_put = function($d){echo $d;} ;
		$this->_putCSV($p) ;
	}
	function downloadCSV($filename,$p=null) {
		$this->_put = function($d){echo $d;} ;
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=' . $filename);
		if($this->param['bom']) echo "\xEF\xBB\xBF" ;
		$this->_putCSV($p) ;
	}
	function saveCSV($filename,$p=null) {
		$fp = fopen($filename,"w") ;
		if(!$fp) return null ;
		if($this->param['bom']) fputs($fp,"\xEF\xBB\xBF") ;		
		$this->_put = function($d) use ($fp){
			fputs($fp,$d) ;
		} ;
		$this->_putCSV($p) ;
		fclose($fp) ;
		return true ;
	}
	function is_hash(&$array) {
	    $i = 0;
	    foreach($array as $k => $dummy) {
	        if ( $k !== $i++ ) return true;
	    }
		return false;
	}
	
}
if (basename($_SERVER["SCRIPT_NAME"]) == basename(__FILE__)) {
	$o = new csv ;
	
	$o->setHead( array('カラム1'=>"col1",'カラム2'=>'col2','カラム3'=>'col3') ) ;
	$o->setLine( array( 1,2,3 ) ) ;
	$o->setLine( array('col1'=>"あ",'col2'=>"い",'col3'=>"う") ) ;
	$o->setLine( array('col2'=>"aa\"a") ) ;	

	echo "<pre>"; $o->showCSV(array('charset'=>"cp932",'delimiter'=>",",'header'=>true)) ;	

//	$o->downloadCSV("test.csv",array('charset'=>"cp932")) ;	

//	$o->saveCSV("../data/test.csv",array('charset'=>"UTF-8")) ;	

}
