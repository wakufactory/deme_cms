<?php
require_once "Database_cms.inc" ;

class api {
	//年表リスト編集用
	function chron_list($p ) {
		$tb = new Table_cms_chron ;

		$q = $p ;
		$sort = "order by year,month,day, mtime desc" ;
		$r = $tb->query_records("*",$q,$sort) ;
//s	print_r($r) ;
		return $r ;
	}
	
	//年表編集よう
	function get_chron($id,$view=false) {
		$tb = new Table_cms_chron ;
		$r = $tb->query_record("*",intval($id)) ;
//		print_r($r) ;
		$c = $r['content'] ;

		return $r ;
	}		
	
	//コンテンツリスト編集用
	function cont_list($p ) {
		$tb = new Table_cms_cont ;

		$q = $p ;
		$sort = "order by date desc, mtime desc" ;
		$r = $tb->query_records("*",$q,$sort) ;
//s	print_r($r) ;
		return $r ;
	}
	
	//コンテンツ編集よう
	function get_cont($id,$view=false) {
		$tb = new Table_cms_cont ;
		$r = $tb->query_record("*",intval($id)) ;
//		print_r($r) ;
		$c = $r['content'] ;

		return $r ;
	}		
		
	
	
	
	
	
	//ページID指定公開用
	function get_page_open($pageid,$view=true) {
		$tb = new Table_cms_page ;
		$r = $tb->query_records("*",array('pageid'=>$pageid)) ;
		if(count($r)==0) return null ;
		$r = $r[0] ;
		if($view) {
			foreach($r['content'] as $k=>$v) {
				$r['content'][$k] = $tb->get_disp($k,$v) ;
				$r['content'][$k."_raw"] = $v ;
			}
			$sn = intval(substr($r['content']['labelimg'],1,1)) ;
			$ss = "" ;
			for($i=0;$i<$sn;$i++) $ss .= "<span></span>" ;
			$r['content']['stars'] = $ss ;
			if($r['content']['company_onoff']==0) unset($r['content']['company_onoff']) ;
			if($r['content']['gtype']==0) unset($r['content']['gtype']);
			if($r['content']['photo0']==0) unset($r['content']['photo0']) ;
			if($r['content']['photo1']==0) unset($r['content']['photo1']) ;
			if($r['content']['photo2']==0) unset($r['content']['photo2']) ;
			if($r['content']['photo3']==0) unset($r['content']['photo3']) ;
			if($r['content']['sitepdf']==0) unset($r['content']['sitepdf']) ;
			if($r['content']['sitepdf_re']==0) unset($r['content']['sitepdf_re']) ;
		}
		return $r ;		
	}	
	//photo ページ生成用
	function get_pnews($pageid,$open=true,$nid=0,$ofs=0,$limit=10) {
		$tb = new Table_cms_news ;
		$st = $open?"(stat = ".STAT_OPEN." ":"(stat != 4 " ;
		if($nid!=0) $st .= " or id = ".intval($nid) ;
		$st .= ") and pageid = '".$pageid."'" ;
//		echo  $st ;
		$r = $tb->query_records("*",$st,"order by date desc,mtime desc limit $limit offset $ofs" ) ;
		for($i=0;$i<count($r);$i++) {
			foreach($r[$i]['content'] as $k=>$v) {
				$r[$i]['content'][$k] = $tb->get_disp($k,$v) ;
				$r[$i]['content'][$k."_raw"] = $v ;
			}	
			$r[$i]['new'] = (time()-strtotime($r[$i]['content']['date']))<10*24*3600?"new":"" ;
			$r[$i]['type'] = array($r[$i]['content']['type']=>1) ;
			if($r[$i]['content']['type']!=1) $r[$i]['pdf'] = str_replace("jpg","pdf",$r[$i]['content']['img_1']);
		}	
		return $r ;
	}	
	//公開ページリスト
	function page_list_open($p,$view=true,$random=false ) {
		$tb = new Table_cms_page ;
		$p['stat']=1 ;
		$ord = $random?"rand()":"ord" ;
		$r = $tb->query_records("*",$p,"order by ".$ord) ;
		for($i=0;$i<count($r);$i++) {
			$fresh = $r[$i]['content']['freshness'] ;
			if($fresh!="") $r[$i]['content']['freshness'] = "[".$fresh."]";
			if($fresh=="新規") $r[$i]['content']['fresh_class'] = "text-red" ;
			if($r[$i]['content']['sitepdf_re']==0) unset($r[$i]['content']['sitepdf_re']) ;
			if($r[$i]['content']['company_onoff']==0) unset($r[$i]['content']['company_onoff']) ;
		}
		return $r ;
	}	
	//photoリスト公開用
	function news_list_open($p=null) {
		$tb = new Table_cms_news ;
		$sort = "order by iine+iineb desc,date desc" ;
		$offset = ($p['offset']>0)?" offset ".intval($p['offset']):"" ;
		$limit = ($p['limit']>0)?" limit ".intval($p['limit']):"" ;
		$r = $tb->query_records("cms_news.*,cms_page.stat,cms_page.content as pc",array('cms_news.stat'=>STAT_OPEN,'type'=>1,'cms_page.stat'=>STAT_OPEN),$sort.$limit.$offset,null,null,"join cms_page on (cms_news.pageid = cms_page.pageid) ") ;
		$ta = array("A"=>"grow","B"=>"oacis","C"=>"create") ;
		for($i=0;$i<count($r);$i++) {
			$c = $r[$i]['content'] ;
			foreach($c as $k=>$v) {
				$r[$i]['content'][$k] = $tb->get_disp($k,$v) ;
				$r[$i]['content'][$k."_raw"] = $v ;
				$r[$i]['type_class'] = $ta[substr($r[$i]['pageid'],0,1)] ;
			}
			$pc = json_decode($r[$i]['pc']) ;
			$r[$i]['sitename'] = $pc->sitename_j ;
			if(substr($r[$i]['pageid'],0,1)=="A") $r[$i]['sitename'] = $pc->company_0." ".$pc->sitename_j  ;
			$r[$i]['new'] = (time()-strtotime($c['open_date']))<10*24*3600?1:0 ;
			$r[$i]['img_1_t'] = str_replace(".jpg","_t.jpg",$r[$i]['content']['img_1']) ;	
			$r[$i]['img_1_s'] = str_replace(".jpg","_s.jpg",$r[$i]['content']['img_1']) ;
		}
		return $r ;
	}		
	
		
	//ぺーじリスト編集
	function page_list_edit($p,$view=false ) {
		$tb = new Table_cms_page ;
		$q = array('cat'=>$p["cat"]) ;
		$r = $tb->query_records("*",$q,"order by ord") ;
		if($view) {
			for($i=0;$i<count($r);$i++) {
				$c = $r[$i]['content'] ;
				foreach($c as $k=>$v) {
					$r[$i]['content'][$k] = $tb->get_disp($k,$v) ;
				}
				$r[$i]['stat_disp'] = $tb->get_disp('stat',$r[$i]['stat']) ;
			}
		}
		return $r ;
	}
	//ページ編集よう
	function get_page($id,$view=false) {
		$tb = new Table_cms_page ;
		$r = $tb->query_record("*",intval($id)) ;
//		print_r($r) ;
		if($view) {
			foreach($r['content'] as $k=>$v) {
				$r['content'][$k] = $tb->get_disp($k,$v) ;
			}
			$r['content']['weblink'] =  $this->ext_link($r['content']['webpage']) ;
		}
		return $r ;
	}

	//ぺーじ削除
	function delete_page($pageid,$stat,$id=null) {
//		echo "del $pageid $stat" ;
		$tb = new Table_cms_page ;
		$r = $tb->query_records("id",array('pageid'=>$pageid,'stat'=>$stat)) ;
		for($i=0;$i<count($r);$i++) {
			if($r[$i]['id']==$id) continue; 
			$tb->delete_record($r[$i]['id']) ;
		}
	}
	
	
	//サイトリスト選択取得
	function page_list_select( ) {
		$tb = new Table_cms_page ;
		$p = array('stat'=>1) ;
		$r = $tb->query_records("*",$p,"order by pageid") ;
		$s = array() ;
		for($i=0;$i<count($r);$i++) {
			$s[$r[$i]['pageid']] = $r[$i]['content']['company_0'] ;
		}
		return $s ;
	}	



	//blogリスト編集用
	function blog_list_edit($k,$view=false ) {
		$tb = new Table_cms_blog ;
		$q = array('kind'=>$k) ;
		$sort = "order by date desc ,mtime desc" ;
		$r = $tb->query_records("*",$q,$sort) ;
//		echo $k;print_r($r) ;
		if($view) {
			for($i=0;$i<count($r);$i++) {
				$c = $r[$i]['content'] ;
				foreach($c as $k=>$v) {
					$r[$i]['content'][$k] = $tb->get_disp($k,$v) ;
				}
			}
		}
		return $r ;
	}
	//blogリスト公開
	function blog_list_open($k,$view=false,$limit=0 ) {
		$tb = new Table_cms_blog ;
		$q = array('kind'=>$k,'stat'=>1) ;
		$sort = "order by date desc ,mtime desc" ;
		$limit = ($limit!=0)?" limit $limit ":"" ;
		$r = $tb->query_records("*",$q,$sort.$limit) ;
//		echo $k;print_r($r) ;
		if($view) {
			for($i=0;$i<count($r);$i++) {
				$c = $r[$i]['content'] ;
				foreach($c as $k=>$v) {
					$r[$i]['content'][$k] = $tb->get_disp($k,$v) ;
				}
			}
		}
		return $r ;
	}
	//blogリストアーカイブ
	function blog_list_arc($k,$ym) {
		$tb = new Table_cms_blog ;
		$k = intval($k) ;
		$m = $tb->_database->select('SELECT distinct date_format(date,"%Y%m") as ym from cms_blog where stat=1 and kind='.$k.' order by date desc') ;
		$ml = $tb->_database->select('select * from cms_blog where stat=1 and kind='.$k.' and date_format(date,"%Y%m")="'.$ym.'" order by date desc,mtime desc') ;
		return array('m'=>$m,'ml'=>$ml) ;
	}
	//blog編集よう
	function get_blog($id,$view=false) {
		$tb = new Table_cms_blog ;
		$r = $tb->query_record("*",intval($id)) ;
//		print_r($r) ;
		if($view) {
			foreach($r['content'] as $k=>$v) {
				$r['content'][$k] = $tb->get_disp($k,$v) ;
			}
		}
		return $r ;
	}
//-----------------------------------------

	function fname($sid,$sfx,$id,$ext) {
		return "p_".$sid."_".$id.".".$ext ;
	}
	function upfile($id,$pageid,$c,$name,&$type) {
		$fn = null ;
		$type=0 ;
		if($_FILES['c']['error'][$name]==0) {
			$pi = pathinfo($_FILES['c']['name'][$name]) ;
			$ext = strtolower($pi['extension']) ;
			$fn = $this->fname($pageid,$name,$id,$ext) ;
			$td = $GLOBALS['UPLOAD_IMAGE_PATH'].$fn ;
			copy($_FILES['c']['tmp_name'][$name],$td) ;	
			chdir($GLOBALS['UPLOAD_IMAGE_PATH']) ;

//			echo "upload $fn" ;	
			if($ext=="pdf") {
				$jfn = $this->fname($pageid,$name,$id,"jpg") ;
				$ex = "convert '".$fn."[0]' -resize 200x -resize 'x200>' -gravity center ../img/watermark_pdf_s.png -composite  $jfn" ;
				exec($ex) ;
				$fn = $jfn ;
				$type=2;
			} else {
				$tfn = $this->fname($pageid,$name,$id."_t","jpg") ;
				$sfn = $this->fname($pageid,$name,$id."_s","jpg") ;
				exec( "convert $fn -auto-orient -strip -resize '200x' -resize 'x200>'   $tfn") ;
				exec( "convert $fn -auto-orient -strip -resize '200x200^'  -gravity center -crop 200x200+0+0 $sfn") ;
				exec( "convert $fn -auto-orient -strip -resize 700x -resize 'x700>'   -gravity southwest ../img/watermark_seges_m.png -composite $fn") ;	
				$type=1 ;			
			}
		} else if($_FILES['c']['error'][$name]==4) $fn = $c[$name] ;
		else $fn = "" ;
		return $fn ;
	}
	function delfile($pageid,$id) {
		chdir($GLOBALS['UPLOAD_IMAGE_PATH']) ;	
		@unlink("p_".$pageid."_".$id.".jpg") ;
		@unlink("p_".$pageid."_".$id."_s.jpg") ;
		@unlink("p_".$pageid."_".$id."_t.jpg") ;
		@unlink("p_".$pageid."_".$id.".pdf") ;
	}
}