<?php
require_once "OhbaTemplate.inc" ;
require_once "auth.inc" ;
require_once "env.inc" ;

class admin_page extends SessionPage {
	
	var $hid ;
	function admin_page() {
		session_set_cookie_params(0.5*3600) ;
		SessionPage::__construct() ;
		//ログインチェック
		$a = new auth() ;
		if($a->Check()<0) {
			header("Location:index.php") ;
		}
		$this->mode = $_SESSION['cred']['auth']['mode'] ;
		$this->name = $_SESSION['cred']['auth']['name'] ;
		$admin = array('user_name'=>$this->name) ;
		if($this->mode==0 || $this->mode==2|| $this->mode==3) $admin['mode_s'] = 1 ;
		$this->admin = $admin ;
//		print_r( $_SESSION['cred']['auth'] );
	}
	
	//会員ログ
	function putlog($msg) {
		global $LOG_PATH ;
		$fp = fopen($LOG_PATH."k".date("Ymd").".log","a") ;
		flock($fp,LOCK_EX) ;
		fputs($fp,date("His")."\t".$this->hid."\t".$msg."\t".$_SERVER['REMOTE_ADDR']."\n") ;
		flock($fp,LOCK_UN) ;
		fclose($fp) ;
	}

	//inputフィールド作成
	function mkftag($tag,$name,$val=null,$attr=null,$sel=null) {
		$t = "" ;
		$at = "" ;
		if($attr) {
			foreach($attr as $k=>$v) {
				if($v!==null) $at .= " ".$k.'="'.$v.'"' ;
			}
		}
		switch($tag) {
			case 'text':
				$val = htmlspecialchars($val) ;
				$t = '<input type="text" name="'.$name.'" value="'.$val.'"'.$at." />" ;
				break ;
			case 'textarea':
				$val = htmlspecialchars($val) ;
				$t = '<textarea name="'.$name.'" '.$at.'>'.$val.'</textarea>' ;
				break ;
			case 'select':
				$t = '<select name="'.$name.'" '.$at.'>' ;
				foreach($sel as $n=>$v) {
					if($n=="-") $n="選択しない" ;
					$t .= '<option value="'.$v.'" '.
						((($v===""&&($val===""||$val===null))||($val!==null&&$val!==""&&$val==$v))?"selected":"").
						'>'.$n.'</option>' ;
				}
				$t .= '</select>' ;
				break ;
			case 'radio':
				$t = '' ;
				foreach($sel as $n=>$v) {
					if($n=="-") $n="選択しない" ;
					$t .= '<input type=radio name="'.$name.'" value="'.$v.'" '.
						((($v===""&&($val===""||$val===null))||($val!==null&&$val!==""&&$val==$v))?"checked":"").
						'>'.$n ;
				}
				break ;
			case 'checkbox':
				$t = '' ;
				if($sel) {
					foreach($sel as $n=>$v) {
						$t .= '<input type=checkbox name="'.$name.'[]" value="'.$v.'" '.
						(($val!==null&&array_search($v,$val)!==false)?"checked":"").
						'>'.$n ;
					}
				} else {
					$t = '<input type=checkbox name="'.$name.'" '.($val?"checked":"").'>' ;
				}
				break ;
			case 'file':
				$t = '<input type=file name="'.$name.'" />' ;
				break ;
			case 'hidden':
				$t = '<input type=hidden name="'.$name.'" value="'.$val.'">' ;
				break ;
		}
		return $t ;	
	}
}