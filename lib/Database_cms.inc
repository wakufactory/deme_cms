<?php
require_once "env.inc" ;
require_once 'OhbaDatabaseMySQL.inc';

/**
 * cms データベースクラス
 */
class Database_cms extends MySqlDatabase {
	var $_dbname ;
	var $_user ;
	var $_password ;
	var $_host ;
	//var $_port = '';
	var $nolog = false ;
	function __construct() {
		$this->_host = $GLOBALS['DB_HOST'] ;
		$this->_dbname = $GLOBALS['DB_NAME'] ;
		$this->_user = $GLOBALS['DB_USER'];
		$this->_password = $GLOBALS['DB_PASS'];	
		
		MySqlDatabase::__construct() ;	
	}

	function error_log($err,$sql) {
		global $LOG_PATH ;
		$f = $LOG_PATH."db_error_".date("Ymd") ;
		$fp = @fopen($f,"a") ;
		flock($fp,LOCK_EX);
		fputs($fp,date("His")."\t".$_SERVER['PHP_SELF'].$_SERVER['QUERY_STRING']."\t".$err."\t".$sql."\n") ;
		flock($fp,LOCK_UN);
		fclose($fp) ;
		chmod($f,0777) ;
	}
	// for debug 
	function sql_log($sql,$time) {
		if($this->nolog) return ;
		global $LOG_PATH ;
		$f = $LOG_PATH."sql_".date("Ymd") ;
		$fp = fopen($f,"a") ;
		flock($fp,LOCK_EX);
		fputs($fp,date("His")."\t".$_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING']."\t".$time."\t".$sql."\n") ;
		flock($fp,LOCK_UN);
		fclose($fp) ;
		@chmod($f,0777) ;
	}	
	

}

class myTable extends OhbaTable {
	
	function __construct($nolog=false) {
		$this->_database = new Database_cms();
		$this->_database->nolog = $nolog ;
	}
	
	//検索キーの正規化
	function skey_norm($k) {
		 return str_replace("，","",str_replace("・","",str_replace(" ","",str_replace("　","",mb_convert_kana(strip_tags(trim($k)),"aKV"))))) ;
	}
	
	//表示形式の取得
	function get_disp($key,$value) {
		$f = $this->_fsel[$key] ;
		if($f) {
			foreach($f as $k=>$v) {
				if($v==$value) return $k ;
			}
		}
		return $value ;
	}
	//年月形式の正規化
	function ym_norm($ym) {
		switch(count(explode("-",$ym))) {
			case 1:
				$ym  = $ym."01" ;
				break ;						
			case 2:
				$ym  = $ym."-01" ;
				break ;
			default:
				$ym  = $ym;
		}	
		return $ym ;	
	}
	function dt_norm($dt) {
		return (strtotime($dt)===FALSE)?null:date("Y-m-d H:i:s",strtotime($dt)) ;
	}
}

define('STAT_OPEN',1) ;
define('STAT_AWAIT',2) ;
define('STAT_OWAIT',3) ;
define('STAT_CLOSED',4) ;


/**
 * chronテーブルクラス
 */
class Table_cms_chron extends myTable {
	var $_table_name = 'cms_chron';
	var $_pkey = "id" ;
	var $_field_types = array(
			'id' => 'serial',
			'uid'=> 'integer',
			'stat' => 'integer',
			'cat' => 'integer',
			'date'=>'date',
			'year'=>'integer',
			'month'=>'integer',
			'day'=>'integer',
			'skey'=>'varchar',
			'content'=>'json',
			'mtime'=>'datetime'
		);
	//var $_delete_field_name = '';
	var $_fsel = array(
		'stat'=>array("非公開"=>0,"公開"=>1),
		'cat'=>array("社内"=>1,"社外"=>2),
	);

	function _set_fld($fld) {
		if(array_key_exists("content", $fld)) {
			$c = $fld['content'] ;

		}
		return $fld ;	
	}
	function insert($fld) {
		$fld = $this->_set_fld($fld) ;
		if(!$fld['mtime']) $fld['mtime'] = date('Y-m-d H:i:s') ;
		return $this->insert_record($fld) ;
	}	
	function update($fld,$id,$ufld="id",$mdate=true) {
		if($id==0) return null ;
		$fld = $this->_set_fld($fld) ;
		if($mdate && !$fld['mtime']) $fld['mtime'] = date('Y-m-d H:i:s') ;
//		print_r($fld) ;
		return $this->update_record($fld,$id,$ufld) ;
	}
}

define('KIND_page',1) ;
define('KIND_EVENT',2) ;
define('KIND_ANN',3) ;
/**
 * newsテーブルクラス
 */
class Table_cms_cont extends myTable {
	var $_table_name = 'cms_cont';
	var $_pkey = "id" ;
	var $_field_types = array(
			'id' => 'serial',
			'uid'=> 'integer',
			'cat'=>'integer',
			'stat' => 'integer',
			'mtime'=>'datetime',
			'date'=>'date',
			'skey'=>'varchar',
			'content'=>'json',
		);
	//var $_delete_field_name = '';
	var $_fsel = array(
		'stat'=>array("下書き"=>0,"公開"=>1,"削除"=>4),

	);

	function _set_fld($fld) {
		if(array_key_exists("content", $fld)) {
			$c = $fld['content'] ;
		}
		return $fld ;	
	}
	function insert($fld) {
		$fld = $this->_set_fld($fld) ;
		if(!$fld['date']) $fld['date'] = date('Y-m-d') ;
		if(!$fld['mtime']) $fld['mtime'] = date('Y-m-d H:i:s') ;
		return $this->insert_record($fld) ;
	}	
	function update($fld,$id,$ufld="id",$mdate=true) {
		if($id==0) return null ;
		$fld = $this->_set_fld($fld) ;
		if(!$fld['date']) $fld['date'] = $fld['content']['date'];
		if($mdate && !$fld['mtime']) $fld['mtime'] = date('Y-m-d H:i:s') ;
		return $this->update_record($fld,$id,$ufld) ;
	}
}

/**
 * blogテーブルクラス
 */
class Table_cms_blog extends myTable {
	var $_table_name = 'cms_blog';
	var $_pkey = "id" ;
	var $_field_types = array(
			'id' => 'serial',
			'kind'=>'integer',
			'pageid'=>'varchar',
			'stat' => 'integer',
			'otime'=>'datetime',
			'etime'=>'datetime',
			'ltime'=>'datetime',
			'mtime'=>'datetime',
			'date'=>'date',
			'skey'=>'varchar',
			'content'=>'json',
		);
	//var $_delete_field_name = '';
	var $_fsel = array(
		'kind'=>array('サイト情報'=>1,'事務局だより'=>2),
		'stat'=>array("下書き"=>0,"公開"=>1,"削除"=>4)
	);

	function _set_fld($fld) {
		if(array_key_exists("content", $fld)) {
			$c = $fld['content'] ;
			if(!$fld['pageid']) $fld['pageid'] = $c['pageid'] ;
			$fld['stat'] = $c['stat'] ;
		}
		return $fld ;	
	}
	function insert($fld) {
		$fld = $this->_set_fld($fld) ;
		if(!$fld['date']) $fld['date'] = date('Y-m-d') ;
		if(!$fld['mtime']) $fld['mtime'] = date('Y-m-d H:i:s') ;
		return $this->insert_record($fld) ;
	}	
	function update($fld,$id,$ufld="id",$mdate=true) {
		if($id==0) return null ;
		$fld = $this->_set_fld($fld) ;
		if(!$fld['date']) $fld['date'] = $fld['content']['date'];
		if($mdate && !$fld['mtime']) $fld['mtime'] = date('Y-m-d H:i:s') ;
		return $this->update_record($fld,$id,$ufld) ;
	}
}


/**
 * Auth テーブルクラス
 */
class Table_Auth extends myTable {
	var $_table_name = 'cms_auth';
	var $_pkey = "serial" ;
	var $_field_types = array(
			'serial' => 'serial',
			'uid' => 'integer',
			'mode' => 'integer',
			'name' => 'varchar',
			'account' => 'varchar',
			'hash'=>'varchar',
			'lastlogin'=>'datetime',
			'text'=>'varchar'
		);
	//var $_delete_field_name = '';
		var $_fsel = array(
			'mode'=>array("admin"=>0,"ユーザ"=>1)
		);

}



?>
