<?php
//フォーム入力 バリデートクラス
require_once "Database_cms.inc" ;

class validate {
	
	public $vf ;
	
	function __construct($kind) {
		$self = $this ;
		$vf =array(
			
		0=> array(
			'shopid'=>array('m'=>"店舗番号は数字5文字で入れてください",'v'=>function($v) {
				return preg_match("/^[0-9]{5}$/",$v) ;
			}),
			'kukaku'=>array(
				array('m'=>"区画番号は必須です",'v'=>"chk_must"),
				array('m'=>"区画番号は英数字6文字で入れてください",'v'=>"chk_ank") ),
			'name_yomi'=>array('o'=>true,'m'=>"店舗よみがなは全角カナで入れてください",'v'=>"chk_kana"),
			'open_date'=>array('m'=>"掲載開始日付けが正しくありません",'v'=>"chk_date"),
		),
		KIND_NEWS=>array(
			
		)
		);
		
		$this->vf = $vf[$kind] ;
	}
	function chk_fld(&$msg,$k,$v,$of=null) {

		$ret = null ;
		if($this->vf[$k]) {
			$aa = $this->vf[$k] ;
			if(!$aa[0]) $aa = array($aa) ;
			$msg = "" ;
			for($i=0;$i<count($aa);$i++) {
				$a = $aa[$i] ;
//				if(!$a['o'] && $v==="") continue ;
				$f = $a['v'] ;
				if(is_string($f)) 
						$r = $this->$f($v) ;
				else 
						$r = $f($v,$of) ;
				if(!($r)) {
					$msg = $a['m'] ;
					return 1 ;
				}
			}
		}
		
		return $ret ;
	}
	
	function conv_fld($k,$v) {
		if(is_array($v)) return $v ;
		$ks = "KV" ;
		$c = ($this->vf[$k][0])?$this->vf[$k][0]['c']:$this->vf[$k]['c'] ;
		if(strpos($c,"n")!==false) $ks .= "ns" ;
		if(strpos($c,"a")!==false) $ks .= "rs" ;
		$r = str_replace("\r\n","\n",trim(mb_convert_kana($v,$ks))) ;
		return $r ;
	}
	function chk_must($v) {
		return trim($v) !== "" ;
	}
	function chk_int($v) {
		return $v=="" || preg_match("/^[0-9]+$/",$v) ;
	}
	function chk_float($v) {
		return $v=="" || preg_match("/^[0-9\.]+$/",$v) ;
	}
	function chk_kana($v) {
		return $v=="" || preg_match("/^([ァ-ヺー・，,\ 　]+)$/u",$v) ;
	}
	function chk_romaji($v) {
		return $v=="" || preg_match("/^([a-z0-9\ ]+)$/i",$v) ;
	}
	function chk_ank($v) {
		return $v=="" || preg_match("/^([a-z0-9]+)$/i",$v) ;
	}
	function chk_date($v) {
		if($v=="") return true ;
		$ret = false ;
		if(preg_match("/^([0-9]{4})[\-\/]?([0-9]{1,2})[\-\/]?([0-9]{1,2})$/",$v,$a)) {
			if(checkdate($a[2],$a[3],$a[1])!==FALSE) $ret = true ;
		}
		return $ret ;
	}
	function chk_email($v) {
		return $v=="" || preg_match("/^[0-9a-z\-\._]+@[a-z0-9\-]+\.[a-z0-9\-\.]+$/i",$v) ;
	}
	
	
}