<?php
require_once "OhbaTemplate.inc" ;
require_once "Database_cms.inc" ;

/**
 * Autorization class
 *
 * if  $_SESSION[cred][auth] set? -> loged in
 */
class Auth {
	/**
	 * constructor
	 */
	function Auth(){
		SessionManager::start();
	}
	
	function set_hash($serial,$account,$passwd) {
		$hash = sha1( $account.$passwd ) ;
//		echo $hash;
		$mt = new Table_Auth;
		return $mt->update_record(array('hash'=>$hash),$serial,'serial') ;		
	}
	function query($account) {
		$mt = new Table_Auth;
		return $mt->query_record("*",$account,'account') ;		
	}
	function Login($account,$passwd,$ctime=0){
		$mt = new Table_Auth;
		$hash = sha1( $account.$passwd ) ;
//		$r = $mt->query_records("account,hid,serial,lastlogin,name",array('account'=>$account, 'passwd'=>$passwd));
		$r = $mt->query_records("mode,serial,lastlogin,account,name",array('account'=>$account, 'hash'=>$hash));
		$cnt=count($r);
		if ($cnt==1) {
			return $this->set_login($r[0]);
		}
		return -1;
	}

	function set_login(&$r)
	{
		session_regenerate_id(TRUE);
		$_SESSION['cred']['auth'] = $r;

		$mt = new Table_Auth;
		if ($mt->update_record(array('lastlogin'=>date('Y-m-d H:i:s')),$r['serial'],"serial")==0){
			return -1;
		}
		return $_SESSION['cred']['auth']['serial'];
	}

	function Logout() {

		unset ($_SESSION['cred']);
		session_destroy() ;
		return 0;
	}

	function Check() {

		if (is_array($_SESSION['cred'])){
			return $_SESSION['cred']['auth']['serial'];
		}
		return -1;
	}
}
?>
