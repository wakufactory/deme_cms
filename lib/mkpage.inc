<?php
require_once "env.inc" ;
require_once "TemplateComponent.inc" ;
require_once "dba.php" ;

class mkpage {
	function make($tdir,$temp,$data,$tname,$save,$op = null ) {
		chdir($GLOBALS['DOCROOT'].$tdir) ;
		$av = array('d'=>$data,'imgbase'=>$GLOBALS['IMAGE_PATH']) ;
		if(!$save) $av['pv']=true ;
		if($op) $av = array_merge($av,$op) ;
//print_r($av) ;
		$t = new TemplateComponent($temp,$av) ;
		$s = $t->getString() ;
		if($save) {
//			echo "save page $tname" ;
			$fp = fopen($tname,"w") ;
			fputs($fp,$s) ;
			fclose($fp) ;
		} ;
		return $s ;			
	}
	//全ページ生成
	function update_all() {
		
	}
	//とっぷぺーじ
	function top_page($save,$mobile=false) {
		$a = new api ;
		$p = $a->news_list_open(array("limit"=>($moblie)?10:20)) ;
		$n1 = $a->blog_list_open(1,true) ;
		$n2 = $a->blog_list_open(2,true,10) ;
		$s = $a->page_list_open(array(),true,$mobile) ;
		$sa = array() ; $sl = array() ; $sr = array() ;
		for($i=0;$i<count($s);$i++) {
			$comp = "" ;
			if($s[$i]['cat']==1  && $s[$i]['content']['company_onoff']==1) {
				$comp = $s[$i]['content']['company_0']."<br/>" ;
			} 
			$s[$i]['sitename'] = $comp.$s[$i]['content']['sitename_j'] ;
			if($s[$i]['content']['photo0']==1) {
				 if($i&1) $sr[] = $s[$i] ;
				 else $sl[] = $s[$i] ;
				 $sa[] = $s[$i] ;
			}
		}
		if($mobile) $sa = array_slice($sa,0,10);
		$mobile = ($mobile)?"mobile":"" ;
		return $this->make($mobile,"_index.html",array('s'=>$sa,'sr'=>$sr,'sl'=>$sl,'n1'=>$n1,'n2'=>$n2,'p'=>$p),"index.html",$save) ;
	}
	//ページ
	function page($pageid,$save=false,$mobile=false) {
		$a = new api ;
//echo "mkpage $pageid" ;
		$r = $a->get_page_open($pageid,true) ;
		if(count($r)==0) return null ;
		$news = $a->get_pnews($r['pageid'],true) ;
		$cat = substr($r['pageid'],0,1) ;
		if(count($news)>0) $r['news'] = $news ;	
		$mobile = ($mobile)?"mobile/":"" ;	
		return $this->make($mobile."site/","_".$cat."0000.html",$r,$r['pageid'].".html",$save) ;	
	}
	function page_pv($pageid,$nid,$mobile=false) {
		$a = new api ;
//echo "mkpage $pageid" ;
		$r = $a->get_page_open($pageid,true) ;
		if(count($r)==0) return null ;
		$news = $a->get_pnews($r['pageid'],true,$nid) ;
		$cat = substr($r['pageid'],0,1) ;
		if(count($news)>0) $r['news'] = $news ;
//echo"<pre>";print_r($r) ;echo"</pre>";
		$mobile = ($mobile)?"mobile/":"" ;	
		return $this->make($mobile."site/","_".$cat."0000.html",$r,$r['pageid'].".html",false) ;	
	}
	//pageインデックス生成
	function page_index($cat,$save,$mobile=false) {
		$a = new api ;
		$tb = new Table_cms_page ;
		$p = array('cat'=>$cat) ;
		$r = $a->page_list_open($p,true) ;
		$cat = substr($r[0]['pageid'],0,1) ;
//print_r($r) ;
		$mobile = ($mobile)?"mobile/":"" ;	
		return $this->make($mobile."site","_".$cat."_index.html",array('l'=>$r),$cat."_index.html",$save) ;
	}
	//page map
	function page_map($save) {
		$a = new api ;
		$r = array() ;
		for($c=1;$c<=3;$c++) {
			$c1 = $a->page_list_open(array('cat'=>$c),true) ;
			for($i=0;$i<count($c1);$i++) $r[] = array(
				'pageid'=>$c1[$i]['pageid'],
				'label'=>$c1[$i]['content']['labelimg'],
				'lat'=>$c1[$i]['content']['map_lat'],
				'lng'=>$c1[$i]['content']['map_lng'],
				'sitename'=>$c1[$i]['content']['company_0']."<br/>".$c1[$i]['content']['sitename_j']
			) ;
		}
		
		return $this->make("site","_segesmap.html",array('points'=>json_encode($r,JSON_UNESCAPED_UNICODE)),"segesmap.html",$save) ;

	}
	//phpo いんでっくす
	function photo_index($save,$mobile=false) {
		$a = new api ;
		$r = $a->news_list_open(array("limit"=>($mobile)?10:60)) ;
		$mobile = ($mobile)?"mobile/":"" ;
		return $this->make($mobile."site","_segesphoto.html", array('l'=>$r), "segesphoto.html",$save) ;
	}
	//site インデック
	function site_index($save) {
		$a = new api ;
		$c1 = $a->page_list_open(array('cat'=>1),true) ;
		$c2 = $a->page_list_open(array('cat'=>2),true) ;
		$c3 = $a->page_list_open(array('cat'=>3),true) ;
		return $this->make("site","_index.html",array('l1'=>$c1,'l2'=>$c2,'l3'=>$c3),"index.html",$save) ;
	}
	
	//infoとっぷ
	function info_index($id,$month,$save=false) {
		$a = new api ;
		if(!$month) $month = date("Ym") ;
		if($id>0) {
			$dd = $a->get_blog($id) ;
			$month = substr($dd['date'],0,4).substr($dd['date'],5,2) ;
		}
		$arc = $a->blog_list_arc(2,$month) ;
		if($id==0) {
			$id =$arc['ml'][0]['id'] ;
			$dd = $a->get_blog($id) ;
		}
		$ml = array() ;
		for($i=0;$i<count($arc['m']);$i++) {
			$ym = $arc['m'][$i]['ym'] ;
			$m = substr($ym,0,4)."-".substr($ym,4,2) ;
			if($ym!=$month) $m = '<a href="archive.php?month='.$ym.'">'.$m.'</a>' ;
			$ml[] = array('link'=>$m) ;
		}
		$tm = array() ;
		for($i=0;$i<count($arc['ml']);$i++) {
			$d = $arc['ml'][$i] ;
			$c = json_decode($d['content'],JSON_UNESCAPED_UNICODE) ;
			$m = $d['date']." ".$c['title'] ;
			if($d['id']!=$id) $m = '<a href="archive.php?id='.$d['id'].'">'.$m.'</a>' ;
			$tm[] = array('link'=>$m) ;
		}
		$dd['content']['body'] = nl2br($dd['content']['body']) ;
		return $this->make("info","_index.html",array('d'=>$dd,'month'=>$tm,'arc'=>$ml),"index.html",$save) ;
	}
}