<?php
/**
 * @brief データベース接続管理（プール）クラス
 *
 * シングルトンパターンを用いているため直接 new 演算子で作成してはならない <br>
 * 例：$database_connection = MySqlDatabaseManager::get_connection( <br>
 *　　　 'データベース名', <br>
 *　　　 array( <br>
 *　　　　 'user=ユーザ名', <br>
 *　　　 ) <br>
 *　　 );
 */
class MySqlDatabaseManager {
        /**
         * データベース接続を返す
         *
         * @param $options データベース接続のオプション配列 <br>
         * 例：array( <br>
         *　　　 'dbname=データベース名', <br>
         *　　　 'user=ユーザ名', <br>
         *　　 )
         * @return pg_connectの返すリソース
         */
        function get_connection($host,$username,$password,$dbname) {
                static $connections=array();

                if (!array_key_exists($dbname,$connections)) {

                        if(!is_null($port))
                        {
                                $host .= ":$port";
                        }

                        $connection = mysqli_connect($host,$username,$password);

                        if (! $connection) {
	                        	$this->error_log("connection error".mysqli_error($connection),"");
                                print "cannot connect database '$connection_string'<br>\n";
                                exit;
                        }
                        
                        mysqli_set_charset( $connection,"utf8mb4");

                        mysqli_select_db($connection,$dbname)
                         or die ("Can't use $dbname!<br>". mysqli_error($connection));

                        if (!is_null($options['names'])){
                                mysqli_query($connection,"SET NAMES ".$options['names']);
                        }

                        $connections[$dbname] = $connection;
                }

                return $connections[$dbname];
        }
}

/**
 * PostgreSQL用データベースクラス（ベースクラス：継承して用いる）
 */
class MySqlDatabase {
        var $_host;     ///データベースサーバ名
        var $_port;     /// データベースのTCPポート番号
        var $_dbname;   /// データベース名:継承したクラスで再定義する
        var $_user;     /// データベースに接続するユーザ名
        var $_password; /// データベースに接続するユーザのパスワード

        var $_connection;       /// データベース接続
        


        /**
         * コンストラクタ
         */
        function MySqlDatabase() {
//              if ($this->_host) {
//                      $options[] = "host=$this->_host";
//              }
//              if ($this->_port) {
//                      $options[] = "port=$this->_port";
//              }
//              if ($this->_user) {
//                      $options[] = "user=$this->_user";
//              }
//              if ($this->_password) {
//                      $options[] = "password=$this->_password";
//              }


                if(empty($this->_port)){
                        $this->_connection = MySqlDatabaseManager::get_connection($this->_host,$this->_user,$this->_password,$this->_dbname);
                }else{
                        $this->_connection = MySqlDatabaseManager::get_connection($this->_host,$this->_user,$this->_password,$this->_dbname,$this->_port);
                }
        }

        /**
         * SELECT SQL 文を実行して結果を返す
         *
         * @param $sql 実行する SQL 文
         * @param $key_field_name 戻り値の配列のキーにするフィールド名
         * @return 実行結果（テーブルのフィールド名をキーにした連想配列の配列）<br>
         * 例：<br>
         *　 テーブル構造<br>
         *　　 CREATE TABLE "member" (<br>
         *　　　 "mem_id" serial,<br>
         *　　　 "mem_name" text<br>
         *　　 );<br>
         *　 $key_field_nameに'mem_id'を指定した場合の戻り値<br>
         *　　 array(<br>
         *　　　 53 => array(<br>
         *　　　　 'mem_id' => 53,<br>
         *　　　　 'mem_name' => '大庭',<br>
         *　　　 ),<br>
         *　　　 63 => array(<br>
         *　　　　 'mem_id' => 63,<br>
         *　　　　 'mem_name' => '石田',<br>
         *　　　 ),<br>
         *　　　 ...<br>
         *　　 )<br>
         *　 $key_field_nameを指定しない場合の戻り値<br>
         *　　 array(<br>
         *　　　 0 => array(<br>
         *　　　　 'mem_id' => 53,<br>
         *　　　　 'mem_name' => '大庭',<br>
         *　　　 ),<br>
         *　　　 1 => array(<br>
         *　　　　 'mem_id' => 63,<br>
         *　　　　 'mem_name' => '石田',<br>
         *　　　 ),<br>
         *　　　 ...<br>
         *　　 )
         */
        function &select($sql, $key_field_name = NULL) {
                $ret = array();
                $result = $this->query($sql);
                if (!$result) return NULL;

                while ($row = mysqli_fetch_assoc($result)) {
                        if ($key_field_name) {
                                $ret[$row[$key_field_name]] = $row;
                        }
                        else {
                                $ret[] = $row;
                        }
                }
                mysqli_free_result($result);
                return $ret;
        }

        /**
         * INSERT SQL 文を実行する
         *
         * @param $sql 実行する SQL 文
         * @param $seq insert後に取得するprimary key fieldのseq
         * @return 挿入したレコードの pkey(あるいはoid)
         */
        function insert($sql,$seq=NULL) {
                $result = $this->query($sql);
                if (!$result) return NULL;

//              if ($seq != ''){
//                      $currval = $this->select("select currval('".$seq."') as seq");
//                      $oid = $currval[0]['seq'];
//
//                      $oid = mysql_insert_id();
//              }
//              else {
//                      $oid = mysql_insert_id();
//              }

                $oid = mysqli_insert_id($this->_connection);
                //mysql_free_result($result);

                return $oid;
        }

        /**
         * UPDATE SQL 文を実行する
         *
         * @param $sql 実行するSQL文
         * @return 更新されたレコード数
         */
        function update($sql) {
                $result= $this->query($sql);
                if (!$result) return NULL;
                $ret = mysqli_affected_rows($this->_connection);
                //mysql_free_result($result);

                return $ret;
        }

        /**
         * DELETE SQL 文を実行する
         *
         * @param $sql 実行するSQL文
         * @return 削除されたレコード数
         */
        function delete($sql) {
                $result = $this->query($sql);
                if (!$result) return NULL;
                $ret = mysqli_affected_rows($this->_connection);
                //mysql_free_result($result);
                return $ret;
        }

        /**
         * 任意のSQL文を実行する
         *
         * @param $sql 実行するSQL文
         * @return 実行結果のリソース（pg_queryの戻り値）<br>
         * ※mysql_free_result()を実行する責任はこの関数を呼んだ側にある
         */
        function query($sql) {
	        	$t = microtime(true) ;
                @$result = mysqli_query($this->_connection,$sql);
				$t = microtime(true)-$t ;
				$this->sql_log($sql,$t) ;
				
                if (! $result) {
                        $err = $this->get_last_error();
//                        echo "SQL:$sql<br>$err<br>";//debug
                        $this->error_log($err,$sql);
                        //exit;
                }

                return $result;
        }

        /**
         * 最後のコマンドのエラーメッセージを得る
         */
        function get_last_error(){
                return mysqli_error($this->_connection);
        }

        /**
         * 値のエスケープ
         */
        function escape_string($s){
                if (function_exists(mysqli_escape_string))
                        return mysqli_escape_string($this->_connection,$s);
                 return AddSlashes($s);
        }
        
        /*エラーログ */
        function error_log($err,$sql) { 
			echo(sprintf("query failed: %s - %s",$err,$sql));
 
        }
        //sql log
        function sql_log($sql,$time) {
	        ;
        }
}

/**
 * テーブルクラス（ベースクラス：継承して用いる）
 */
class OhbaTable {
        var $_table_name;       /// テーブル名:継承したクラスで再定義する
        var $_database;         /// データベースオブジェクト
        var $_pkey='oid';       /// primary key field
        var $_seq_pkey=NULL;    /// sequencer for primary key field
        var $_constraint;       /// constraint condition

        var $_delete_field_name;        /// 削除フラグフィールド

        /**
         * レコードをテーブルに追加する
         *
         * @param $record テーブルのフィールド名をキーにした連想配列
         * @return 追加したレコードのoid
         */
        function insert_record($record) {
                return $this->_insert_record($record);
        }

        function _insert_record($record) {
                if (is_array($record)) {
                        foreach ($record as $field_name => $field_value) {
                                if ($field_name == 'oid') continue;

                                if (!is_null($field_value)) {
                                        $fields[] = "$field_name";
                                        $values[] = $this->_get_quotedstr($field_name,$field_value);
                                }
                        }
                        if (count($fields)) {
                                $sql =
                                        " INSERT INTO `$this->_table_name` " .
                                        ' ( ' . join(',', $fields) . ' ) '.
                                        ' VALUES ' .
                                        ' ( ' . join(',', $values) . ' ) ';

                        }
                        else {
                                $sql = " INSERT INTO `$this->_table_name` DEFAULT VALUES ";
                        }

                        $rv = $this->_database->insert($sql,$this->_seq_pkey);

                        //primary keyでもsequenceを使わない事がある。
                        if ($this->_pkey && $record[$this->_pkey]){
                                return $record[$this->_pkey];
                        }

                        return $rv;
                }
        }

        /**
         * ユニークキーを指定してレコードを取得する
         *
         * @param $fields 取得するレコードのフィールド
         * @param $unique_value ユニークキーの値（$keyを指定しない場合はoid）
         * @param $unique_key ユニークキーのフィールド名（デフォルトはoid）
         * @return 検索結果（テーブルのフィールド名をキーにした連想配列）
         */
        function &query_record($fields, $unique_value, $unique_key = NULL) {
                if (! $unique_value) return array();//unique_valueが無い→empty

                if (! $unique_key) {
                        $unique_key = $this->_pkey;
                }

                $sql =
                        " SELECT $fields FROM `$this->_table_name` " .
                        $this->_where_phrase(array($unique_key=>$unique_value));

                if ($this->_delete_field_name) {
                        $sql .= " AND $this->_delete_field_name IS NULL ";
                }

                $records = $this->_database->select($sql);

                if (0 < count($records)) {
                        $this->_decode_json($records[0]);
                        return $records[0] ;
                }
                else {
                        return array();
                }
        }

        /**
         * 検索条件を指定してレコードを取得する
         *
         * @param $fields 取得するレコードのフィールド
         * @param $where 検索条件（連想配列の場合:空でないフィールドを完全一致条件で検索、文字列の場合:WHERE句として検索条件にする）
         * @param $option ORDER句やLIMIT句などの検索制約条件
         * @param $operators 検索条件に用いる演算子（テーブルのフィールド名をキーにした連想配列）
         * @param $hash_key 検索結果の連想配列のキー
         * @param $join 追加のJOIN句
         * @return 検索結果（テーブルのフィールド名をキーにした連想配列の配列）
         */
        function &query_records(
                $fields, $where = NULL, $option = NULL, $operators = NULL,
                $hash_key = NULL, $join = NULL)
        {
                return $this->_query_records($fields, $where, $option, $operators, $hash_key, $join);
        }

        function &_query_records(
                $fields, $where = NULL, $option = NULL, $operators = NULL,
                $hash_key = NULL, $join = NULL)
        {
                $where_phrase = $this->_where_phrase($where, $operators);

                $sql =
                        " SELECT $fields FROM `$this->_table_name`" .
                        $join." ".$where_phrase." ".$option;

                //for DEBUG
                //print "<pre>$sql\r\n</pre>";//exit();

                $ret = $this->_database->select($sql, $hash_key);
                for($i=0;$i<count($ret);$i++) {
	                $this->_decode_json($ret[$i]) ;
                }
                return $ret ;
        }
        
        // decode json fields 
        function _decode_json(&$flds) {
			foreach($flds as $f=>$v) {
			    if($this->_field_types[$f] == "json") $flds[$f] = json_decode($v,true) ;
			}	        
        }

        /**
         * 配列からSQLのwhere句を形成する
         *
         * @param $where 検索条件（連想配列の場合:空でないフィールドを完全一致条件で検索、文字列の場合:WHERE句として検索条件にする）
         * @param $operators 検索条件に用いる演算子（テーブルのフィールド名をキーにした連想配列）
         * @return SQLのwhere句
         */
        function _where_phrase($where = NULL, $operators = NULL) {
                if (is_array($where)) {
                        foreach ($where as $field_name => $field_values) {
                                // get operator
                                if (is_array($operators)
                                 && $operators[$field_name]!=''){
                                        $op = $operators[$field_name];
                                }
                                else {
                                        $op='=';
                                }

                                if (is_array($field_values)){
                                        $field_value=$field_values[0];
                                }
                                else {
                                        $field_value=$field_values;
                                }

                                if (!is_null($field_value)) {
                                        if (preg_match('/^[=<>~]/',$op)
                                         && $field_value ==='') continue;

                                        switch(strtolower($op)){
                                        case 'between':
                                                if (!is_array($field_values)) break;

                                                $sets[] = ''.$field_name.' '.
$op.' '.$this->_get_quotedstr($field_name, $field_values[0]).
' and '.$this->_get_quotedstr($field_name, $field_values[1]);
                                                break;

                                        case 'in':
                                                if (!is_array($field_values)) break;
                                                $in_val=array();
                                                foreach ($field_values as $v){
                                                        $in_val[] = $this->_get_quotedstr($field_name, $v);
                                                }

                                                $sets[] = ''.$field_name.' '.  $op.' ('.join(',',$in_val).')';
                                                unset($in_val);
                                                break;

                                        default:
                                                $sets[] = ''.$field_name.' '.$op.$this->_get_quotedstr($field_name, $field_value);
                                                break;
                                        }
                                }
                                else {
                                        if ($op=='='){
                                                $sets[] = ''.$field_name.' is NULL';
                                        }
                                        elseif ($op=='!=' || $op=='<>'){
                                                $sets[] = ''.$field_name.' is not NULL';
                                        }
                                        else {
                                                $sets[] = ''.$field_name.' '.$op;
                                        }
                                }
                        }

                }
                elseif ($where != ''){
                        $sets[] = $where;
                }

                if ($this->_constraint){
                        $sets[] = $this->_constraint;
                }

                if (count($sets)) {
                        return ' WHERE '.join(' AND ', $sets);
                }
                return '';
        }

        /**
         * レコードを更新する
         *
         * @param $record 更新情報を設定した連想配列（テーブルのフィールド名をキーにした）
         * @param $unique_value ユニークキーの値（省略した場合は$recordに設定してあると仮定する）
         * @param $unique_key ユニークキー（省略した場合は_pkey）
         * @return 更新したレコード数
         */
        function update_record(
                $record, $unique_value = NULL, $unique_key = NULL)
        {
                return $this->_update_record($record, $unique_value, $unique_key);
        }

        function _update_record(
                $record, $unique_value = NULL, $unique_key = NULL)
        {
                if (! $unique_key) {
                        $unique_key = $this->_pkey;
                }
                if (! $unique_value){
                        if (is_array($record) && array_key_exists($unique_key, $record)){
                                $unique_value = $record[$unique_key];
                        }
                        else return 0;
                }

                return $this->update_records($record,"$unique_key = ".$this->_get_quotedstr($unique_key,$unique_value));
        }

        /**
         * レコードを更新する
         * @param $record 更新情報。テーブルのフィールド名をキーにした連想配列かSQL式。
         * @param $where where条件。テーブルのフィールド名をキーにした連想配列かSQL式。
         * @param $operators where条件が連想配列の演算子。
         */
        function update_records($record, $where, $operators = NULL) {
                $res = 0;

                $where_phrase = $this->_where_phrase($where, $operators);

                if ($where_phrase == ''){
                        //全てのレコードが変更対象になってしまうので禁止。
                        return 0;
                }

                if (is_array($record)) {
                        foreach ($record as $field_name => $field_value) {
                                if ($field_name == 'oid') continue;

                                if (is_null($field_value)) {
                                        $sets[] = ''.$field_name.'=NULL';
                                }
                                else {
                                        $sets[] = ''.$field_name.'='.$this->_get_quotedstr($field_name, $field_value);
                                }
                        }
                        $set_phrase = join(',', $sets);
                }
                else {
                        $set_phrase = $record;
                }
                if ($set_phrase){
                        $sql = 'UPDATE `'.$this->_table_name.'` SET '.$set_phrase.' '.$where_phrase;
                        $res = $this->_database->update($sql);
                }

                return $res;
        }

        /**
         * レコードを削除する
         *
         * @param $unique_value ユニークキーの値（省略した場合は$recordに設定してあると仮定する）
         * @param $unique_key ユニークキー（省略した場合は_pkey）
         * @return 削除したレコード数
         */
        function delete_record($unique_value, $unique_key = NULL) {
                if (! $unique_key) {
                        $unique_key = $this->_pkey;
                }

                if (! $unique_value){//Where is empty
                        return 0;
                }

                $where = $this->_where_phrase(array($unique_key=>$unique_value));
                if ($where == '') return 0;

                $sql = " DELETE FROM `$this->_table_name` " . $where;
                return $this->_database->delete($sql);
        }

        /**
         * フィールドの引用済文字列を返す
         * @param $field_name フィールド名
         * @param $field_val フィールド値
         * @return 引用済文字列
         */
        function _get_quotedstr($field_name,$field_val) {
                if ($field_name === 'oid'){
                        return intval($field_val);
                }

                if (preg_match("/(\w+)_array/", $this->_field_types[$field_name])) {
                        //if (is_array($field_val)){
                                $field_val = $this->_get_array_field_value($field_val);
                        //}
                        return "'".$this->_database->escape_string($field_val)."'";
                }

                switch ($this->_field_types[$field_name]) {
                case 'bigint':
                case 'int8':
                case 'integer':
                case 'int':
                case 'int2':
                case 'int4':
                case 'oid':
                case 'smallint':
                case 'serial':
                case 'bigserial':
                        return $field_val===''?'NULL':intval($field_val);
                        break;

                case 'double precision':
                case 'float':
                case 'float8':
                case 'real':
                case 'float4':
                case 'double':
                case 'numeric':
                case 'decimal':
                        return $field_val===''?'NULL':doubleval($field_val);
                        break;

                case 'boolean':
                case 'bool':
                        switch ($field_val){
                        case 't':
                        case 'T':
                        case '1':
                        case 'TRUE':
                        case 'true':
                        case 'checked':
                        case 'selected':
                                return 'TRUE';
                                break;

                        case 'f':
                        case 'F':
                        case '0':
                        case 'FALSE':
                        case 'false':
                                return 'FALSE';
                                break;

                        case 'NULL':
                                return 'NULL';
                                break;

                        default:
                                return intval($field_val)?'TURE':'FALSE';
                        }
                        break;

                case 'text':
                case 'varchar':
                case 'char':
                        return "'".$this->_database->escape_string($field_val)."'";
                        break;

                //空文字列を許さない型→空文字列はNULLにする。
                case 'date':
                case 'interval':
                case 'time':
                case 'timestamp':
                case 'datetime':
                        if (!strcasecmp($field_val,'now')) return 'CURRENT_TIMESTAMP';
                        //fall through

                case 'inet':
                        if ($field_val === '') return 'NULL';
                        return "'".$this->_database->escape_string($field_val)."'";
                        break;
                // encode json fields 
				case 'json':
					if(!defined('JSON_UNESCAPED_UNICODE')) define('JSON_UNESCAPED_UNICODE',0) ;
					return "'".$this->_database->escape_string(json_encode($field_val,JSON_UNESCAPED_UNICODE))."'";
					break ;
                default:
                        break;
                }

                return "'".$this->_database->escape_string($field_val)."'";
        }


        /**
         * 配列フィールドの値を整形する
         *
         * @param $field_value
         * @return 整形された文字列
         */
        function _get_array_field_value($field_value) {
                if (is_array($field_value)) {
                        foreach ($field_value as $value) {
                                if (is_array($value)) {
                                        $values[] = $this->_get_array_field_value($value);
                                }
                                else {
                                        $values[] = $value;
                                }
                        }
                }
                else {
                        $values[] = $field_value;
                }

                return '{' . (is_array($values)?join(',', $values):''). '}';
        }
}

/**
 * 選択肢項目用 値置換えのベースクラス（ベースクラス：継承して用いる）
 */
class PgXlat {
        /**
         * 入力フォーム用の値置き換え
         */
        function xlat_form($table_object,&$args,$table) {
                foreach ($table_object->_field_types as $k => $v){
                        if ($k == $table_object->_pkey) continue;

                        //特別に置き換えたくないフィールドがあるなら除外
                        //if ($k == "... ") continue;

                        $m = "enum_".$k;
                        if (!method_exists($this,$m)) continue;

                        $args[enum][$k]=$this->$m($args[$table][$k]);
                        //$args[enum][$table][$k]=$this->$m($args[$table][$k]);
                }
        }

        function _cb_xlat(&$v,$k,$a){
                $v[$a[field]]=$this->$a[m]($v[$a[field]]);
        }

        function xlat_list($table_object,&$args,$tables){
                foreach ($table_object->_field_types as $k => $v){
                        if ($k == $table_object->_pkey) continue;

                        $m = "rx_".$k;
                        if (!method_exists($this,$m)) continue;
                        if (is_array($args[$tables])) array_walk($args[$tables],array($this,'_cb_xlat'),array('m'=>$m,'field'=>$k));
                }
        }

        function xlat_confirm($table_object,&$args,$table){
                foreach ($table_object->_field_types as $k => $v){
                        if ($k == $table_object->_pkey) continue;

                        $m = "rx_".$k;
                        if (!method_exists($this,$m)) continue;
                        $args[$table][$k]=$this->$m($args[$table][$k]);
                }
        }
}
?>