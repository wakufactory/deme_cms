<?php
require_once "auth.inc" ;
session_start() ;
$p = $_REQUEST ;

$a = new Auth ;
if($p['login'] && $p['account']) {
	if($a->Login($p['account'],$p['password'])>0) {
		llog("success ".$p['account']) ;
		Header("Location:index.php") ;
	} else {
		llog("fail ".$p['account']) ;		
	}
} else if($p['logout']) {
	$a->Logout() ;
}
function llog($msg) {
	global $LOG_PATH ;
	$f = $LOG_PATH."login_".date("Ymd") ;
	$fp = fopen($f,"a") ;
	flock($fp,LOCK_EX);
	fputs($fp,date("Y-m-d H:i:s")."\t".$msg."\t".$_SERVER['REMOTE_ADDR']."\t".$_SERVER['HTTP_USER_AGENT']."\n") ;
	flock($fp,LOCK_UN);
	fclose($fp) ;
	chmod($f,0777) ;
}
?>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div style="margin:50px 50px">
<h1>GF LOGIN</h1>
<form method=post>
アカウント <input type=text name=account /><br/><br/>
パスワード  <input type=password name=password /><br/><br/>
<input type=submit name=login value="LOGIN">　
<input type=submit name=logout value="LOGOUT">
</form>
</div>