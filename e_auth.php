<?php
require_once "admin_page.inc" ;
require_once "auth.inc" ;

class auth_page extends admin_page {
	
	//リストページ表示
	function On_default($p) {
		$tb = new Table_Auth()  ;
		$q = array() ;
		if($this->mode!=0) $q = "mode != 0" ;
		$l = $tb->query_records("*",$q,"order by mode,account") ;
//print_r($d) ;
		for($i=0;$i<count($l);$i++) {
			$l[$i]['mode'] = $tb->get_disp("mode",$l[$i]['mode']) ;
		}
		$d = array('l'=>$l) ;
		return array('auth_l_t.html',$d) ;
	}
	
	//編集ページ表示
	function On_edit($p) {
		$id = $p['get']['id'] ;
		$db = new Table_Auth ;
		$d = $db->query_record("*",$id) ;
//print_r($d) ;
		return $this->editpage($d) ;
	}
	
	//新規ページ表示
	function On_new($p) {
		$d = array('mode'=>1) ;
		return $this->editpage($d) ;
	}
	function editpage($d) {
		$db = new Table_Auth ;

		$f = $this->mkform($d) ;
		$p = array('d'=>$d,'f'=>$f) ;
		return array('auth_e_t.html',$p);	
	}
	
	//更新フォームデータ生成
	function mkform($c) {
		$tb = new Table_Auth ;
		$f['account'] = $this->mkftag('text','c[account]',$c['account'], array('size'=>20)) ;
		$f['password'] = $this->mkftag('text','c[password]',$c['password'], array('size'=>20)) ;
		$f['name'] = $this->mkftag('text','c[name]',$c['name'], array('size'=>20)) ;
		$sel = $tb->_fsel['mode'] ;
		if($this->mode!=0) array_shift($sel) ;
		$f['mode'] = $this->mkftag('select','c[mode]',$c['mode'],null,$sel) ;
		return $f ;		
	}
	function On_doupdate($p) {
//		echo "update" ;
		$p = $p['post'] ;
//	print_r($p) ;
		$db = new Table_Auth ;
		$id = $p['serial'] ;
		if($id==0) {
			$id = $db->insert_record(array('account'=>"")) ;
		}		
		$c = $p['c'] ;
		unset($c['password']) ;

		$db->update_record($c,$id) ;
		$a = new Auth ;
		$a->set_hash($id,$c['account'],$p['c']['password']) ;
		
		Header("Location:?") ;
	}
	function On_delete($p) {
		$id = intval($p['get']['id']) ;
		if($id>0) {
			$db = new Table_Auth() ;
			$db->delete_record($id) ;
		}
		Header("Location:?") ;
	}
}
$o = new auth_page ;
$o->dispatch();